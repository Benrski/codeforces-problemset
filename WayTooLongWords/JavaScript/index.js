var wordsCount = parseInt(readline());
var words = [];

for (var i = 0; i < wordsCount; i++) {
  words.push(readline());
}

for (var i = 0; i < words.length; i++) {
  var word = words[i];
  var wordLength = word.length;
  if (wordLength <= 10) {
    print(word);
  } else {
    print(word.charAt(0) + (wordLength - 2) + word.charAt(wordLength - 1));
  }
}

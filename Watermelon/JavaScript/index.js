var n = parseInt(readline());

if (n % 2 === 0 && n > 2) {
  print("YES");
} else {
  print("NO");
}
